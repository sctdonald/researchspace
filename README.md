# ResearchSpace Backend

this backend is written with Java with springboot.
I have perpared some of the api call for fronend usage so that the lab technical can use the frontend to get the sample results.

1. Get all Sample that can be visible by the target username.
2. Get Sample Detail by specific Sample Id.
3. Get all SubSample that can be visible by the target username.
4. Get SubSample Detail by specific SubSample Id.

I decided to Make the Api endpoint in Controller.
And The controller will call the corresponding services, although this time I do not need to deal with the logic, i still implemented this layer as I like to keep the layering clearer.
And the services will call the external services API, where I get the info details from ResearchSpace. 
And return to the frontend with the corresponding result. As I do not changed any data fields or column from the result, I decided to use the Model defined return to frontend directly.

I have set the port to 1001, if the port needed to be changed, please change it in application.yml

