package com.researchspace.sample.config.properties.researchSpace;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class ResearchSpaceServiceUriProperties {
    @NotEmpty
    private String getSample;

    @NotEmpty
    private String getSampleDetail;

    @NotEmpty
    private String getSampleRevision;

    @NotEmpty
    private String getSampleRevisionDetail;

    @NotEmpty
    private String getSubSample;

    @NotEmpty
    private String getSubSampleDetail;

    @NotEmpty
    private String getSubSampleRevision;

    @NotEmpty
    private String getSubSampleRevisionDetail;
}
