package com.researchspace.sample.config.properties.researchSpace;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ResearchSpaceServiceConfigProperties {
    @NotEmpty
    private String baseUri;

    @NotEmpty
    private String apiKey;

    @NotNull
    private ResearchSpaceServiceUriProperties uri;
}
