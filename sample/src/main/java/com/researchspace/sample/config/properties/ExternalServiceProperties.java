package com.researchspace.sample.config.properties;

import com.researchspace.sample.config.properties.researchSpace.ResearchSpaceServiceConfigProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix = "external-service")
@Validated
@Data
public class ExternalServiceProperties {

    private ResearchSpaceServiceConfigProperties researchSpaceService;
}
