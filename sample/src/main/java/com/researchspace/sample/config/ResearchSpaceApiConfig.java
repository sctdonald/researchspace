package com.researchspace.sample.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ResearchSpaceApiConfig {
    @Bean("ResearchSpaceApiRestTemplate")
    public RestTemplate getResearchSpaceApiRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();

        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(getResearchSpaceApiObjectMapper());

        restTemplate.getMessageConverters().add(0, converter);
        return restTemplate;
    }

    @Bean("ResearchSpaceApiObjectMapper")
    public ObjectMapper getResearchSpaceApiObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        return new ObjectMapper();
    }
}
