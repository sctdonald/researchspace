package com.researchspace.sample.error;


public class GeneralError extends BasicError{
    public static final GeneralError
            unexpectedError = new GeneralError(9999, "Unexpected exception");

    private GeneralError(int sequence, String errorMessage) {
        super(sequence, errorMessage);
    }
}
