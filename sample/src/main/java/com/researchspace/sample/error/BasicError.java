package com.researchspace.sample.error;

import java.util.Objects;

public class BasicError {
    protected String source;
    protected String errorCode;
    protected String errorMessage;
    protected String extra;

    public BasicError(String source, String errorCode, String errorMessage, String extra) {
        this.source = source;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.extra = extra;
    }

    public BasicError(int sequence, String errorMessage) {
        this.source = null;
        this.errorCode = PREFIX() + String.format("%04d", sequence);
        this.errorMessage = errorMessage;
    }

    public String PREFIX() {
        return "E";
    }

    public String getSource() {
        return source;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getExtra() {
        return extra;
    }

    public BasicError withExtra(String extraMessage) {
        BasicError output;
        try {
            output = (BasicError) this.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return this;
        }
        output.extra = extraMessage;
        return output;
    }

    public BasicError withExtra(String key, Object value) {
        return withExtra(key + "=" + value);
    }

    private boolean isWellConfigurated() {
        return errorCode != null && errorMessage != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BasicError)) return false;
        BasicError that = (BasicError) o;
        return Objects.equals(errorCode, that.errorCode);
    }

    @Override
    public int hashCode() {

        return Objects.hash(errorCode);
    }

    @Override
    public String toString() {
        return "BasicError{" +
                "source='" + source + '\'' +
                ", errorCode='" + errorCode + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", extra='" + extra + '\'' +
                '}';
    }
}
