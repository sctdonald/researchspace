package com.researchspace.sample.error;

import org.springframework.http.HttpStatus;

public class InternalServerException extends ResponseException {
    public InternalServerException(BasicError exception) {
        super(HttpStatus.INTERNAL_SERVER_ERROR, exception);
    }
}
