package com.researchspace.sample.error;

import org.springframework.http.HttpStatus;

public class ResponseException extends RuntimeException {

    private HttpStatus responseStatus;
    private BasicError error;

    public ResponseException(HttpStatus responseStatus, BasicError error) {
        super("[" + error.getErrorCode() + "]" + error.getErrorMessage() + "(extra: " + error.getExtra() + ")");
        this.responseStatus = responseStatus;
        this.error = error;
    }

    public BasicError getError() {
        return error;
    }

    public HttpStatus getResponseStatus() {
        return responseStatus;
    }

    @Override
    public String toString() {
        return "ResponseException{" +
                "responseStatus=" + responseStatus +
                ", error=" + error +
                '}';
    }
}
