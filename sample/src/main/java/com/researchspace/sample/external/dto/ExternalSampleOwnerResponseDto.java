package com.researchspace.sample.external.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalSampleOwnerResponseDto {
    @JsonProperty("id")
    private Integer id;                         //"id": 3178511,

    @JsonProperty("username")
    private String username;                    //"username": "sctdonald",

    @JsonProperty("email")
    private String email;                       //"email": "sctdonald@gmail.com",

    @JsonProperty("firstName")
    private String firstName;                   //"firstName": "Chak To",

    @JsonProperty("lastName")
    private String lastName;                    //"lastName": "Sin",

    @JsonProperty("homeFolderId")
    private Integer homeFolderId;                //"homeFolderId": 33253,

    @JsonProperty("workbenchId")
    private Integer workbenchId;                //"workbenchId": null,

    @JsonProperty("_links")
    private List<ExternalSampleLinkResponseDto> links;
}
