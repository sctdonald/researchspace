package com.researchspace.sample.external.service.iface;

import com.researchspace.sample.external.dto.*;

public interface ResearchSpaceExtService {
    ExternalGetAllSampleResponseDto getSampleByUserName(String userName, String orderBy, Integer pageNumber);
    ExternalGetSampleDetailResponseDto getSampleBySampleId(Integer sampleId);
    ExternalGetAllSampleRevisionResponseDto getSampleRevisionBySampleId(Integer sampleId);
    ExternalGetRevisionDetailResponseDto getRevisionDetailsByRevisionAndSampleId(Integer sampleId, Integer revisionId);
    ExternalGetAllSubSampleResponseDto getAllSubSampleByUserName(String userName, String orderBy, Integer pageNumber);
    ExternalGetSubSampleDetailResponseDto getSubSampleDetailBySubSampleId(Integer subSampleId);
    ExternalGetAllSubSampleRevisionResponseDto getAllSubSampleRevisionsBySubSampleId(Integer subSampleId);
    ExternalGetSubSampleRevisionDetailResponseDto getSubSampleRevisionDetailByRevisionAndSubSampleId(Integer subSampleId, Integer revisionId);
}
