package com.researchspace.sample.external.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalSampleRevisionResponseDto {
    @JsonProperty("revisionId")
    private Integer revisionId;             //"revisionId": 49224,

    @JsonProperty("revisionType")
    private String revisionType;            //"revisionType": "ADD",

    @JsonProperty("record")
    private ExternalRevisionRecordResponseDto record;
}
