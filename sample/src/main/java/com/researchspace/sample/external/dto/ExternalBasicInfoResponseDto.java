package com.researchspace.sample.external.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalBasicInfoResponseDto {
    @JsonProperty("id")
    private Integer id;                                     //"id": 1081522,

    @JsonProperty("globalId")
    private String globalId;                                //"globalId": "SS1081522",

    @JsonProperty("name")
    private String name;                                    //"name": "My Sample.01",

    @JsonProperty("description")
    private String description;                             //"description": null,

    @JsonProperty("created")
    private String created;                                 //"created": "2021-08-24T12:50:03.000Z",

    @JsonProperty("createdBy")
    private String createdBy;                               //"modifiedBy": "sctdonald",

    @JsonProperty("lastModified")
    private String lastModified;                            //"lastModified": "2021-08-24T12:50:02.655Z",

    @JsonProperty("modifiedBy")
    private String modifiedBy;                              //"modifiedBy": "sctdonald",

    @JsonProperty("modifiedByFullName")
    private String modifiedByFullName;                      //"modifiedByFullName": "Chak To Sin",

    @JsonProperty("deleted")
    private boolean deleted;                                //"deleted": false,

    @JsonProperty("deletedDate")
    private String deletedDate;                             //"deletedDate": null,

    @JsonProperty("revisionId")
    private String revisionId;                              //"revisionId": null,

    @JsonProperty("owner")
    private ExternalSampleOwnerResponseDto owner;

    @JsonProperty("permittedActions")
    private List<String> permittedActions;                  //"UPDATE", "CHANGE_OWNER"

    @JsonProperty("iconId")
    private Integer iconId;                                 //"iconId": -1,

    @JsonProperty("quantity")
    private ExternalSampleQuantityResponseDto quantity;

    @JsonProperty("tags")
    private String tags;                                    //"tags": "test, api",

    @JsonProperty("barcode")
    private String barcode;                                 //"barcode": null,

    @JsonProperty("type")
    private String type;                                    //"type": "SAMPLE",


    @JsonProperty("_links")
    private List<ExternalSampleLinkResponseDto> links;

}
