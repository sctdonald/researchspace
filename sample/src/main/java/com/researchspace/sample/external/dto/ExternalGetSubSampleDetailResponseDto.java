package com.researchspace.sample.external.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalGetSubSampleDetailResponseDto extends ExternalSubSampleResponseDto{
    @JsonProperty("parentLocation")
    private ExternalParentLocationResponseDto parentLocation;

    @JsonProperty("extraFields")
    private List<String> extraFields;

    @JsonProperty("notes")
    private List<String> notes;
}
