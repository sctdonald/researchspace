package com.researchspace.sample.external.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalGetAllSampleRevisionResponseDto {
    @JsonProperty("revisionsCount")
    private Integer revisionsCount;

    @JsonProperty("revisions")
    private List<ExternalSampleRevisionResponseDto> revisions;
}
