package com.researchspace.sample.external.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalSubSampleResponseDto extends ExternalBasicInfoResponseDto{
    @JsonProperty("parentContainers")
    private List<ExternalParentContainerResponseDto> parentContainers;

    @JsonProperty("lastMoveDate")
    private String lastMoveDate;                            //"lastMoveDate": null,

    @JsonProperty("sample")
    private ExternalSampleResponseDto sample;
}
