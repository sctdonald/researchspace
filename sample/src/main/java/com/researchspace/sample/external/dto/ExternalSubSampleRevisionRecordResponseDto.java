package com.researchspace.sample.external.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalSubSampleRevisionRecordResponseDto extends ExternalBasicInfoResponseDto {
    @JsonProperty("sample")
    private ExternalSampleResponseDto sample;

    @JsonProperty("parentContainers")
    private List<ExternalParentContainerResponseDto> parentContainers;

    @JsonProperty("parentLocation")
    private ExternalParentLocationResponseDto parentLocation;
}
