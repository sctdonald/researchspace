package com.researchspace.sample.external.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalGetAllSubSampleResponseDto {
    @JsonProperty("totalHits")
    private Integer totalHits;

    @JsonProperty("pageNumber")
    private Integer pageNumber;

    @JsonProperty("subSamples")
    private List<ExternalSubSampleResponseDto> subSamples;

    @JsonProperty("_links")
    private List<ExternalSampleLinkResponseDto> links;
}
