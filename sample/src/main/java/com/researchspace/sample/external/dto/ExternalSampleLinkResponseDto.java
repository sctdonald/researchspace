package com.researchspace.sample.external.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalSampleLinkResponseDto {
    @JsonProperty("link")
    private String link;                        //"link": "https://demos.researchspace.com/api/inventory/v1/samples/1048667",

    @JsonProperty("rel")
    private String rel;                         //"rel": "self"
}
