package com.researchspace.sample.external.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalRevisionRecordResponseDto extends ExternalBasicInfoResponseDto{
    @JsonProperty("version")
    private Integer version;                    //"version": 1,

    @JsonProperty("historicalVersion")
    private boolean historicalVersion;          //"historicalVersion": false,

    @JsonProperty("templateId")
    private Integer templateId;                 //"templateId": null,

    @JsonProperty("template")
    private boolean template;                   //"template": false,

    @JsonProperty("subSampleAlias")
    private String subSampleAlias;              //"subSampleAlias": "aliquot",

    @JsonProperty("subSamplesCount")
    private Integer subSamplesCount;            //"subSamplesCount": 1,

    @JsonProperty("storageTempMin")
    private Integer storageTempMin;             //"storageTempMin": null,

    @JsonProperty("storageTempMax")
    private Integer storageTempMax;             //"storageTempMax": null,

    @JsonProperty("sampleSource")
    private String sampleSource;                //"sampleSource": "VENDOR_SUPPLIED",

    @JsonProperty("expiryDate")
    private String expiryDate;                  //"expiryDate": null,


}
