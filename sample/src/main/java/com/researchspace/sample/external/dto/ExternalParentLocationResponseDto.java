package com.researchspace.sample.external.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;


@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalParentLocationResponseDto {
    @JsonProperty("id")
    private Integer id;             //"id": 5434,

    @JsonProperty("coordX")
    private Integer coordX;         //"coordX": 1,

    @JsonProperty("coordY")
    private Integer coordY;         //"coordY": 1,
}

