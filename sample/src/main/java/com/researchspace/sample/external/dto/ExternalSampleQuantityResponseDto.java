package com.researchspace.sample.external.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalSampleQuantityResponseDto {
    @JsonProperty("numericValue")
    private Integer numericValue;           //"numericValue": 10,

    @JsonProperty("unitId")
    private Integer unitId;                 //"unitId": 3

}
