package com.researchspace.sample.external.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalSampleContentSummaryResponseDto {
    @JsonProperty("totalCount")
    private Integer totalCount;             //"totalCount": 8,

    @JsonProperty("subSampleCount")
    private Integer subSampleCount;         //"subSampleCount": 8

    @JsonProperty("containerCount")
    private Integer containerCount;         //"containerCount": 0

}
