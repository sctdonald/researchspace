package com.researchspace.sample.external.service.impl;

import com.researchspace.sample.config.properties.ExternalServiceProperties;
import com.researchspace.sample.config.properties.researchSpace.ResearchSpaceServiceConfigProperties;
import com.researchspace.sample.error.GeneralError;
import com.researchspace.sample.error.InternalServerException;
import com.researchspace.sample.external.dto.*;
import com.researchspace.sample.external.service.iface.ResearchSpaceExtService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import com.google.common.collect.ImmutableMap;

@Service
@Slf4j
public class ResearchSpaceExtServiceImpl implements ResearchSpaceExtService {
    private ResearchSpaceServiceConfigProperties researchSpaceApiProperties;
    private RestTemplate restTemplate;

    @Autowired
    public ResearchSpaceExtServiceImpl(ExternalServiceProperties externalServiceProperties, @Qualifier("ResearchSpaceApiRestTemplate") RestTemplate restTemplate){
        this.researchSpaceApiProperties = externalServiceProperties.getResearchSpaceService();
        this.restTemplate = restTemplate;
    }

    @Override
    public ExternalGetAllSampleResponseDto getSampleByUserName(String userName, String orderBy, Integer pageNumber){
        log.info("ResearchSpaceExtServiceImpl getSampleByUserName start: userName={}, orderBy={}, pageNumber=}{}", userName, orderBy, pageNumber);

        String uri = UriComponentsBuilder
                .fromUriString(researchSpaceApiProperties.getBaseUri())
                .path(researchSpaceApiProperties.getUri().getGetSample())
                .buildAndExpand(ImmutableMap.of(
                        "name", userName,
                        "order-by", orderBy,
                        "page-number", pageNumber
                ))
                .toUriString();
        log.info("the complete uri: {}", uri);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("apiKey", researchSpaceApiProperties.getApiKey());
            return restTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    new HttpEntity<>(headers),
                    ExternalGetAllSampleResponseDto.class
            ).getBody();
        } catch (HttpStatusCodeException e) {
            String errorBody = e.getResponseBodyAsString();
            log.error("getSampleByUserName failed with status: {}, body: {}", e.getStatusCode(), errorBody);
            throw new InternalServerException(GeneralError.unexpectedError);
        }
    }

    @Override
    public ExternalGetSampleDetailResponseDto getSampleBySampleId(Integer sampleId){
        log.info("ResearchSpaceExtServiceImpl getSampleBySampleId start: sampleId={}", sampleId);
        String uri = UriComponentsBuilder
                .fromUriString(researchSpaceApiProperties.getBaseUri())
                .path(researchSpaceApiProperties.getUri().getGetSampleDetail())
                .buildAndExpand(ImmutableMap.of(
                        "sampleId", sampleId
                ))
                .toUriString();
        log.info("the complete uri: {}", uri);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("apiKey", researchSpaceApiProperties.getApiKey());
            return restTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    new HttpEntity<>(headers),
                    ExternalGetSampleDetailResponseDto.class
            ).getBody();
        } catch (HttpStatusCodeException e) {
            String errorBody = e.getResponseBodyAsString();
            log.error("getSampleBySampleId failed with status: {}, body: {}", e.getStatusCode(), errorBody);
            throw new InternalServerException(GeneralError.unexpectedError);
        }
    }

    @Override
    public ExternalGetAllSampleRevisionResponseDto getSampleRevisionBySampleId(Integer sampleId){
        log.info("ResearchSpaceExtServiceImpl getSampleRevisionBySampleId start: sampleId={}", sampleId);
        String uri = UriComponentsBuilder
                .fromUriString(researchSpaceApiProperties.getBaseUri())
                .path(researchSpaceApiProperties.getUri().getGetSampleRevision())
                .buildAndExpand(ImmutableMap.of(
                        "sampleId", sampleId
                ))
                .toUriString();
        log.info("the complete uri: {}", uri);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("apiKey", researchSpaceApiProperties.getApiKey());
            return restTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    new HttpEntity<>(headers),
                    ExternalGetAllSampleRevisionResponseDto.class
            ).getBody();
        } catch (HttpStatusCodeException e) {
            String errorBody = e.getResponseBodyAsString();
            log.error("getSampleRevisionBySampleId failed with status: {}, body: {}", e.getStatusCode(), errorBody);
            throw new InternalServerException(GeneralError.unexpectedError);
        }
    }

    @Override
    public ExternalGetRevisionDetailResponseDto getRevisionDetailsByRevisionAndSampleId(Integer sampleId, Integer revisionId){
        log.info("ResearchSpaceExtServiceImpl getRevisionDetailsByRevisionAndSampleId start: sampleId={}, revisionId={}", sampleId, revisionId);
        String uri = UriComponentsBuilder
                .fromUriString(researchSpaceApiProperties.getBaseUri())
                .path(researchSpaceApiProperties.getUri().getGetSampleRevisionDetail())
                .buildAndExpand(ImmutableMap.of(
                        "sampleId", sampleId,
                        "revisionId", revisionId
                ))
                .toUriString();
        log.info("the complete uri: {}", uri);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("apiKey", researchSpaceApiProperties.getApiKey());
            return restTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    new HttpEntity<>(headers),
                    ExternalGetRevisionDetailResponseDto.class
            ).getBody();
        } catch (HttpStatusCodeException e) {
            String errorBody = e.getResponseBodyAsString();
            log.error("getRevisionDetailsByRevisionAndSampleId failed with status: {}, body: {}", e.getStatusCode(), errorBody);
            throw new InternalServerException(GeneralError.unexpectedError);
        }
    }

    @Override
    public ExternalGetAllSubSampleResponseDto getAllSubSampleByUserName(String userName, String orderBy, Integer pageNumber){
        log.info("ResearchSpaceExtServiceImpl getAllSubSampleByUserName start: userName={}, orderBy={}, pageNumber={}", userName, orderBy, pageNumber);

        String uri = UriComponentsBuilder
                .fromUriString(researchSpaceApiProperties.getBaseUri())
                .path(researchSpaceApiProperties.getUri().getGetSubSample())
                .buildAndExpand(ImmutableMap.of(
                        "name", userName,
                        "order-by", orderBy,
                        "page-number", pageNumber
                ))
                .toUriString();
        log.info("the complete uri: {}", uri);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("apiKey", researchSpaceApiProperties.getApiKey());
            return restTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    new HttpEntity<>(headers),
                    ExternalGetAllSubSampleResponseDto.class
            ).getBody();
        } catch (HttpStatusCodeException e) {
            String errorBody = e.getResponseBodyAsString();
            log.error("getAllSubSampleByUserName failed with status: {}, body: {}", e.getStatusCode(), errorBody);
            throw new InternalServerException(GeneralError.unexpectedError);
        }
    }

    @Override
    public ExternalGetSubSampleDetailResponseDto getSubSampleDetailBySubSampleId(Integer subSampleId){
        log.info("ResearchSpaceExtServiceImpl getSubSampleDetailBySubSampleId start: subSampleId={}", subSampleId);
        String uri = UriComponentsBuilder
                .fromUriString(researchSpaceApiProperties.getBaseUri())
                .path(researchSpaceApiProperties.getUri().getGetSubSampleDetail())
                .buildAndExpand(ImmutableMap.of(
                        "subSampleId", subSampleId
                ))
                .toUriString();
        log.info("the complete uri: {}", uri);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("apiKey", researchSpaceApiProperties.getApiKey());
            return restTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    new HttpEntity<>(headers),
                    ExternalGetSubSampleDetailResponseDto.class
            ).getBody();
        } catch (HttpStatusCodeException e) {
            String errorBody = e.getResponseBodyAsString();
            log.error("getSubSampleDetailBySubSampleId failed with status: {}, body: {}", e.getStatusCode(), errorBody);
            throw new InternalServerException(GeneralError.unexpectedError);
        }
    }

    @Override
    public ExternalGetAllSubSampleRevisionResponseDto getAllSubSampleRevisionsBySubSampleId(Integer subSampleId){
        log.info("ResearchSpaceExtServiceImpl getAllSubSampleRevisionsBySubSampleId start: subSampleId={}", subSampleId);
        String uri = UriComponentsBuilder
                .fromUriString(researchSpaceApiProperties.getBaseUri())
                .path(researchSpaceApiProperties.getUri().getGetSubSampleRevision())
                .buildAndExpand(ImmutableMap.of(
                        "subSampleId", subSampleId
                ))
                .toUriString();
        log.info("the complete uri: {}", uri);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("apiKey", researchSpaceApiProperties.getApiKey());
            return restTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    new HttpEntity<>(headers),
                    ExternalGetAllSubSampleRevisionResponseDto.class
            ).getBody();
        } catch (HttpStatusCodeException e) {
            String errorBody = e.getResponseBodyAsString();
            log.error("getAllSubSampleRevisionsBySubSampleId failed with status: {}, body: {}", e.getStatusCode(), errorBody);
            throw new InternalServerException(GeneralError.unexpectedError);
        }
    }

    @Override
    public ExternalGetSubSampleRevisionDetailResponseDto getSubSampleRevisionDetailByRevisionAndSubSampleId(Integer subSampleId, Integer revisionId){
        log.info("ResearchSpaceExtServiceImpl getSubSampleRevisionDetailByRevisionAndSubSampleId start: subSampleId={}, revisionId={}", subSampleId, revisionId);
        String uri = UriComponentsBuilder
                .fromUriString(researchSpaceApiProperties.getBaseUri())
                .path(researchSpaceApiProperties.getUri().getGetSubSampleRevisionDetail())
                .buildAndExpand(ImmutableMap.of(
                        "subSampleId", subSampleId,
                        "revisionId", revisionId
                ))
                .toUriString();
        log.info("the complete uri: {}", uri);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("apiKey", researchSpaceApiProperties.getApiKey());
            return restTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    new HttpEntity<>(headers),
                    ExternalGetSubSampleRevisionDetailResponseDto.class
            ).getBody();
        } catch (HttpStatusCodeException e) {
            String errorBody = e.getResponseBodyAsString();
            log.error("getSubSampleRevisionDetailByRevisionAndSubSampleId failed with status: {}, body: {}", e.getStatusCode(), errorBody);
            throw new InternalServerException(GeneralError.unexpectedError);
        }
    }
}