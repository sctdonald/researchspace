package com.researchspace.sample.external.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalParentContainerResponseDto extends ExternalBasicInfoResponseDto {
    @JsonProperty("locationsCount")
    private Integer locationsCount;

    @JsonProperty("contentSummary")
    private ExternalSampleContentSummaryResponseDto contentSummary;

    @JsonProperty("cType")
    private String cType;                               //"cType": "WORKBENCH",

    @JsonProperty("canStoreSamples")
    private boolean canStoreSamples;                    //"canStoreSamples": true,

    @JsonProperty("canStoreContainers")
    private boolean canStoreContainers;                 //canStoreContainers: true

    @JsonProperty("parentLocation")
    private ExternalParentLocationResponseDto parentLocation;

    @JsonProperty("lastMoveDate")
    private String lastMoveDate;                        //"lastMoveDate": null,

}
