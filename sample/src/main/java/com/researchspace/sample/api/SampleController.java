package com.researchspace.sample.api;

import com.researchspace.sample.external.dto.*;
import com.researchspace.sample.service.iface.SampleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/sample")
@Slf4j
public class SampleController {
    private SampleService sampleService;

    @Autowired
    public SampleController(SampleService sampleService){
        this.sampleService = sampleService;
    }

    @GetMapping("/user/{userName}/get-all-sample")
    private ExternalGetAllSampleResponseDto getAllSampleByUserId(@PathVariable("userName") String userName, @RequestParam("page") Integer page, @RequestParam("orderBy") String orderBy){
        ExternalGetAllSampleResponseDto result = sampleService.getAllSampleByUserName(userName, orderBy, page);
        log.info(String.valueOf(result.getSamples().stream().count()));
        return result;
    }

    @GetMapping("/{sampleId}/get-sample-detail")
    private ExternalGetSampleDetailResponseDto getSampleBySampleId(@PathVariable("sampleId") Integer sampleId){
        return sampleService.getSampleBySampleId(sampleId);
    }

    @GetMapping("/{sampleId}/get-sample-revision")
    private ExternalGetAllSampleRevisionResponseDto getAllSampleRevisionBySampleId(@PathVariable("sampleId") Integer sampleId) {
        return sampleService.getSampleRevisionsBySampleId(sampleId);
    }

    @GetMapping("/{sampleId}/get-sample-revision/{revisionId}")
    private ExternalGetRevisionDetailResponseDto getRevisionDetailByRevisionId(@PathVariable("sampleId") Integer sampleId, @PathVariable("revisionId") Integer revisionId){
        return sampleService.getRevisionDetailByRevisionAndSampleId(sampleId, revisionId);
    }

}
