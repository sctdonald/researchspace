package com.researchspace.sample.api;

import com.researchspace.sample.external.dto.ExternalGetAllSubSampleResponseDto;
import com.researchspace.sample.external.dto.ExternalGetAllSubSampleRevisionResponseDto;
import com.researchspace.sample.external.dto.ExternalGetSubSampleDetailResponseDto;
import com.researchspace.sample.external.dto.ExternalGetSubSampleRevisionDetailResponseDto;
import com.researchspace.sample.service.iface.SubSampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/subSample")
public class SubSampleController {
    private SubSampleService subSampleService;

    @Autowired
    public SubSampleController(SubSampleService subSampleService){
        this.subSampleService = subSampleService;
    }

    @GetMapping("/user/{userName}/get-all-subSamples")
    private ExternalGetAllSubSampleResponseDto getAllSubSampleByUserName(@PathVariable("userName") String userName, @RequestParam("page") Integer page, @RequestParam("orderBy") String orderBy){
        return subSampleService.getAllSubSampleByUserName(userName, orderBy, page);
    }

    @GetMapping("/{subSampleId}/get-subSample-detail")
    private ExternalGetSubSampleDetailResponseDto getSubSampleDetailBySubSampleId(@PathVariable("subSampleId") Integer subSampleId){
        return subSampleService.getSubSampleDetailBySubSampleId(subSampleId);
    }

    @GetMapping("/{subSampleId}/get-all-sub-sample-revision")
    private ExternalGetAllSubSampleRevisionResponseDto getAllSubSampleRevisionBySubSampleId(@PathVariable("subSampleId") Integer subSampleId){
        return subSampleService.getAllSubSampleRevisionsBySubSampleId(subSampleId);
    }

    @GetMapping("/{subSampleId}/get-sub-sample-revision/{revisionId}")
    private ExternalGetSubSampleRevisionDetailResponseDto getSubSampleRevisionDetailByRevisionId(@PathVariable("subSampleId") Integer subSampleId, @PathVariable("revisionId") Integer revisionId){
        return subSampleService.getSubSampleRevisionDetailByRevisionAndSubSampleId(subSampleId, revisionId);
    }
}
