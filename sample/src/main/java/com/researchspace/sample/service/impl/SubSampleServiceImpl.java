package com.researchspace.sample.service.impl;

import com.researchspace.sample.external.dto.ExternalGetAllSubSampleResponseDto;
import com.researchspace.sample.external.dto.ExternalGetAllSubSampleRevisionResponseDto;
import com.researchspace.sample.external.dto.ExternalGetSubSampleDetailResponseDto;
import com.researchspace.sample.external.dto.ExternalGetSubSampleRevisionDetailResponseDto;
import com.researchspace.sample.external.service.iface.ResearchSpaceExtService;
import com.researchspace.sample.service.iface.SubSampleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SubSampleServiceImpl implements SubSampleService {

    private ResearchSpaceExtService researchSpaceExtService;

    @Autowired
    public SubSampleServiceImpl(ResearchSpaceExtService researchSpaceExtService){
        this.researchSpaceExtService = researchSpaceExtService;
    }

    @Override
    public ExternalGetAllSubSampleResponseDto getAllSubSampleByUserName(String userName, String orderBy, Integer pageNumber){
        return researchSpaceExtService.getAllSubSampleByUserName(userName, orderBy, pageNumber);
    }

    @Override
    public ExternalGetSubSampleDetailResponseDto getSubSampleDetailBySubSampleId(Integer subSampleId){
        return researchSpaceExtService.getSubSampleDetailBySubSampleId(subSampleId);
    }

    @Override
    public ExternalGetAllSubSampleRevisionResponseDto getAllSubSampleRevisionsBySubSampleId(Integer subSampleId){
        return researchSpaceExtService.getAllSubSampleRevisionsBySubSampleId(subSampleId);
    }

    @Override
    public ExternalGetSubSampleRevisionDetailResponseDto getSubSampleRevisionDetailByRevisionAndSubSampleId(Integer subSampleId, Integer revisionId){
        return researchSpaceExtService.getSubSampleRevisionDetailByRevisionAndSubSampleId(subSampleId, revisionId);
    }
}
