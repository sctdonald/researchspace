package com.researchspace.sample.service.iface;

import com.researchspace.sample.external.dto.ExternalGetRevisionDetailResponseDto;
import com.researchspace.sample.external.dto.ExternalGetSampleDetailResponseDto;
import com.researchspace.sample.external.dto.ExternalGetAllSampleResponseDto;
import com.researchspace.sample.external.dto.ExternalGetAllSampleRevisionResponseDto;

public interface SampleService {
    ExternalGetAllSampleResponseDto getAllSampleByUserName(String userName, String orderBy, Integer pageNumber);
    ExternalGetSampleDetailResponseDto getSampleBySampleId(Integer sampleId);
    ExternalGetAllSampleRevisionResponseDto getSampleRevisionsBySampleId(Integer sampleId);
    ExternalGetRevisionDetailResponseDto getRevisionDetailByRevisionAndSampleId(Integer sampleId, Integer revision);
}
