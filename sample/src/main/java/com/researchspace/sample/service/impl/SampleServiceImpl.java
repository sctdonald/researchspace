package com.researchspace.sample.service.impl;

import com.researchspace.sample.external.dto.ExternalGetRevisionDetailResponseDto;
import com.researchspace.sample.external.dto.ExternalGetSampleDetailResponseDto;
import com.researchspace.sample.external.dto.ExternalGetAllSampleResponseDto;
import com.researchspace.sample.external.dto.ExternalGetAllSampleRevisionResponseDto;
import com.researchspace.sample.external.service.iface.ResearchSpaceExtService;
import com.researchspace.sample.service.iface.SampleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SampleServiceImpl implements SampleService {
    private ResearchSpaceExtService researchSpaceExtService;

    @Autowired
    public SampleServiceImpl(ResearchSpaceExtService researchSpaceExtService){
        this.researchSpaceExtService = researchSpaceExtService;
    }

    @Override
    public ExternalGetAllSampleResponseDto getAllSampleByUserName(String userName, String orderBy, Integer pageNumber){
        return researchSpaceExtService.getSampleByUserName(userName, orderBy, pageNumber);
    }

    @Override
    public ExternalGetSampleDetailResponseDto getSampleBySampleId(Integer sampleId){
        return researchSpaceExtService.getSampleBySampleId(sampleId);
    }

    @Override
    public ExternalGetAllSampleRevisionResponseDto getSampleRevisionsBySampleId(Integer sampleId){
        return researchSpaceExtService.getSampleRevisionBySampleId(sampleId);
    }

    @Override
    public ExternalGetRevisionDetailResponseDto getRevisionDetailByRevisionAndSampleId(Integer sampleId, Integer revision){
        return researchSpaceExtService.getRevisionDetailsByRevisionAndSampleId(sampleId, revision);
    }
}
