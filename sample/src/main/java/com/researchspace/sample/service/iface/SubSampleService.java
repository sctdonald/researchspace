package com.researchspace.sample.service.iface;

import com.researchspace.sample.external.dto.ExternalGetAllSubSampleResponseDto;
import com.researchspace.sample.external.dto.ExternalGetAllSubSampleRevisionResponseDto;
import com.researchspace.sample.external.dto.ExternalGetSubSampleDetailResponseDto;
import com.researchspace.sample.external.dto.ExternalGetSubSampleRevisionDetailResponseDto;

public interface SubSampleService {
    ExternalGetAllSubSampleResponseDto getAllSubSampleByUserName(String userName, String orderBy, Integer pageNumber);
    ExternalGetSubSampleDetailResponseDto getSubSampleDetailBySubSampleId(Integer subSampleId);
    ExternalGetAllSubSampleRevisionResponseDto getAllSubSampleRevisionsBySubSampleId(Integer subSampleId);
    ExternalGetSubSampleRevisionDetailResponseDto getSubSampleRevisionDetailByRevisionAndSubSampleId(Integer subSampleId, Integer revisionId);

}
